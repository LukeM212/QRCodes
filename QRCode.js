const typeBits = {
	numeric: [ 0, 4, 7, 10 ],
	alphanumeric: [ 0, 6, 11 ],
	byte: [ 0, 8 ]
};

const sizes = {
	M1: {
		types: {
			numeric: {
				modeIndicator: "",
				charCountLength: 3
			}
		},
		correction: [ 'n' ],
		terminator: "000"
	},
	M2: {
		types: {
			numeric: {
				modeIndicator: "0",
				charCountLength: 4
			},
			alphanumeric: {
				modeIndicator: "1",
				charCountLength: 3
			}
		},
		correction: [ 'l', 'm' ],
		terminator: "00000"
	},
	M3: {
		types: {
			numeric: {
				modeIndicator: "00",
				charCountLength: 5
			},
			alphanumeric: {
				modeIndicator: "01",
				charCountLength: 4
			},
			byte: {
				modeIndicator: "10",
				charCountLength: 4
			}
		},
		correction: [ 'l', 'm' ],
		terminator: "0000000"
	},
	M4: {
		types: {
			numeric: {
				modeIndicator: "000",
				charCountLength: 6
			},
			alphanumeric: {
				modeIndicator: "001",
				charCountLength: 5
			},
			byte: {
				modeIndicator: "010",
				charCountLength: 5
			}
		},
		correction: [ 'l', 'm', 'q' ],
		terminator: "000000000"
	},
	V1_9: {
		types: {
			numeric: {
				modeIndicator: "0001",
				charCountLength: 10
			},
			alphanumeric: {
				modeIndicator: "0010",
				charCountLength: 9
			},
			byte: {
				modeIndicator: "0100",
				charCountLength: 8
			}
		},
		correction: [ 'l', 'm', 'q', 'h' ],
		terminator: "0000"
	},
	V10_26: {
		types: {
			numeric: {
				modeIndicator: "0001",
				charCountLength: 12
			},
			alphanumeric: {
				modeIndicator: "0010",
				charCountLength: 11
			},
			byte: {
				modeIndicator: "0100",
				charCountLength: 16
			}
		},
		correction: [ 'l', 'm', 'q', 'h' ],
		terminator: "0000"
	},
	V27_40: {
		types: {
			numeric: {
				modeIndicator: "0001",
				charCountLength: 14
			},
			alphanumeric: {
				modeIndicator: "0010",
				charCountLength: 13
			},
			byte: {
				modeIndicator: "0100",
				charCountLength: 16
			}
		},
		correction: [ 'l', 'm', 'q', 'h' ],
		terminator: "0000"
	}
};

const RSBlocks = {
	M1: [ 5, { n:[1, 2] }, 4 ],
	M2: [ 10, { l:[1, 5], m:[1, 6] } ],
	M3: [ 17, { l:[1, 6], m:[1, 8] }, 4 ],
	M4: [ 24, { l:[1, 8], m:[1, 10], q:[1, 14] } ],
	V1: [ 26, { l:[1, 7], m:[1, 10], q:[1, 13], h:[1, 17] } ],
	V2: [ 44, { l:[1, 10], m:[1, 16], q:[1, 22], h:[1, 28] } ],
	V3: [ 70, { l:[1, 15], m:[1, 26], q:[2, 18], h:[2, 22] } ],
	V4: [ 100, { l:[1, 20], m:[2, 18], q:[2, 26], h:[4, 16] } ],
	V5: [ 134, { l:[1, 26], m:[2, 24], q:[4, 18], h:[4, 22] } ],
	V6: [ 172, { l:[2, 18], m:[4, 16], q:[4, 24], h:[4, 28] } ],
	V7: [ 196, { l:[2, 20], m:[4, 18], q:[6, 14], h:[5, 26] } ],
	V8: [ 242, { l:[2, 24], m:[4, 22], q:[6, 22], h:[6, 26] } ],
	V9: [ 292, { l:[2, 30], m:[5, 22], q:[8, 20], h:[8, 24] } ],
	V10: [ 346, { l:[4, 18], m:[5, 26], q:[8, 24], h:[8, 28] } ],
	V11: [ 404, { l:[4, 20], m:[5, 30], q:[8, 28], h:[11, 24] } ],
	V12: [ 466, { l:[4, 24], m:[8, 22], q:[10, 26], h:[11, 28] } ],
	V13: [ 532, { l:[4, 26], m:[9, 22], q:[12, 24], h:[16, 22] } ],
	V14: [ 581, { l:[4, 30], m:[9, 24], q:[16, 20], h:[16, 24] } ],
	V15: [ 655, { l:[6, 22], m:[10, 24], q:[12, 30], h:[18, 24] } ],
	V16: [ 733, { l:[6, 24], m:[10, 28], q:[17, 24], h:[26, 30] } ],
	V17: [ 815, { l:[6, 28], m:[11, 28], q:[16, 28], h:[19, 28] } ],
	V18: [ 901, { l:[6, 30], m:[13, 26], q:[18, 28], h:[21, 28] } ],
	V19: [ 991, { l:[7, 28], m:[14, 26], q:[21, 26], h:[25, 26] } ],
	V20: [ 1085, { l:[8, 28], m:[16, 26], q:[20, 30], h:[25, 28] } ],
	V21: [ 1156, { l:[8, 28], m:[17, 26], q:[23, 28], h:[25, 30] } ],
	V22: [ 1258, { l:[9, 28], m:[17, 28], q:[23, 30], h:[34, 24] } ],
	V23: [ 1364, { l:[9, 30], m:[18, 28], q:[25, 30], h:[30, 30] } ],
	V24: [ 1474, { l:[10, 30], m:[20, 28], q:[27, 30], h:[32, 30] } ],
	V25: [ 1588, { l:[12, 26], m:[21, 28], q:[29, 30], h:[35, 30] } ],
	V26: [ 1706, { l:[12, 28], m:[23, 28], q:[34, 28], h:[37, 30] } ],
	V27: [ 1828, { l:[12, 30], m:[25, 28], q:[34, 30], h:[40, 30] } ],
	V28: [ 1921, { l:[13, 30], m:[26, 28], q:[35, 30], h:[42, 30] } ],
	V29: [ 2051, { l:[14, 30], m:[28, 28], q:[38, 30], h:[45, 30] } ],
	V30: [ 2185, { l:[15, 30], m:[29, 28], q:[40, 30], h:[48, 30] } ],
	V31: [ 2323, { l:[26, 30], m:[31, 28], q:[43, 30], h:[51, 30] } ],
	V32: [ 2465, { l:[27, 30], m:[33, 28], q:[45, 30], h:[54, 30] } ],
	V33: [ 2611, { l:[18, 30], m:[35, 28], q:[48, 30], h:[57, 30] } ],
	V34: [ 2761, { l:[19, 30], m:[37, 28], q:[51, 30], h:[60, 30] } ],
	V35: [ 1876, { l:[19, 30], m:[38, 28], q:[53, 30], h:[63, 30] } ],
	V36: [ 3034, { l:[20, 30], m:[40, 28], q:[56, 30], h:[66, 30] } ],
	V37: [ 3196, { l:[21, 30], m:[43, 28], q:[59, 30], h:[70, 30] } ],
	V38: [ 3362, { l:[22, 30], m:[45, 28], q:[62, 30], h:[74, 30] } ],
	V39: [ 3532, { l:[24, 30], m:[47, 28], q:[65, 30], h:[77, 30] } ],
	V40: [ 3706, { l:[25, 30], m:[49, 28], q:[68, 30], h:[81, 30] } ]
};

const masks = {
	M: [
		(x, y) => y % 2 == 0,
		(x, y) => (Math.floor(y / 2) + Math.floor(x / 3)) % 2 == 0,
		(x, y) => (((y * x) % 2) + ((y * x) % 3)) % 2 == 0,
		(x, y) => (((y + x) % 2) + ((y * x) % 3)) % 2 == 0
	],
	V: [
		(x, y) => (y + x) % 2 == 0,
		(x, y) => y % 2 == 0,
		(x, y) => x % 3 == 0,
		(x, y) => (y + x) % 3 == 0,
		(x, y) => (Math.floor(y / 2) + Math.floor(x / 3)) % 2 == 0,
		(x, y) => ((y * x) % 2) + ((y * x) % 3) == 0,
		(x, y) => (((y * x) % 2) + ((y * x) % 3)) % 2 == 0,
		(x, y) => (((y + x) % 2) + ((y * x) % 3)) % 2 == 0
	]
};

var gf = { };
gf.exp = [ ];
gf.log = [ ];
for (var x = 1, i = 0; i < 255; i++) {
	gf.exp[i] = x;
	gf.log[x] = i;
	x <<= 1;
	if (x >= 256) x ^= 0b100011101;
}
gf.poly_mult = function(p1, p2) {
	var result = Array(p1.length + p2.length - 1).fill(0);
	
	for (var i2 = 0; i2 < p2.length; i2++) if (p2[i2])
		for (var i1 = 0; i1 < p1.length; i1++) if (p1[i1])
			result[i1 + i2] ^= gf.exp[(gf.log[p1[i1]] + gf.log[p2[i2]]) % 255];
	
	return result;
};
gf.poly_rem = function(p1, p2) {
	var result = p1.slice();
	
	for (var i = 0; i < p1.length + 1 - p2.length; i++)
		gf.poly_mult(p2, [ result[i] ]).slice(1).forEach((c, j) => result[i + j + 1] ^= c);
	
	return result.slice(p1.length + 1 - p2.length); // result = result.slice(0, p1.length + 1 - p2.length)
};
gf.bin_poly_rem = function(p1, p2) {
	var remSize = Math.floor(Math.log2(p2));
	for (var i = Math.floor(Math.log2(p1)) - remSize; i >= 0; i--)
		if (p1 & (1 << (remSize + i)))
			p1 ^= p2 << i;
	return p1;
};

function GetQRCode(data, correction, allowMicro) { // Todo: Allow setting a specific size
	function inType(data, type) {
		if (data == undefined || data.length == 0) return false;
		switch(type) {
			case 'byte': return data.split('').every(c => c.charCodeAt() < 256);
			case 'alphanumeric': return data.split('').every(c => "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".indexOf(c) >= 0);
			case 'numeric': return data.split('').every(c => "0123456789".indexOf(c) >= 0);
		}
	}
	
	var modeData = { };
	
	// Set up modeData with supported versions
	for (var size in sizes) {
		var type = [ 'byte', 'alphanumeric', 'numeric' ].find(t => t in sizes[size].types);
		if ((size.charAt(0) == 'V' || allowMicro) &&
			sizes[size].correction.indexOf(correction) >= 0 &&
			inType(data, type))
			modeData[size] = [ { type: type, data: data } ];
	}
	
	// Replace bytes with alphanumeric where size would be decreased
	// Replace alphanumeric / bytes with numeric where size would be decreased
	// Replace alphanumeric fragments with bytes where size would be decreased
	for (var replacement of [
		{ from: 'byte', to: 'alphanumeric' },
		{ from: 'byte', to: 'numeric' },
		{ from: 'alphanumeric', to: 'numeric' },
		{ from: 'alphanumeric', to: 'byte' }
	]) for (var size in modeData)
		if (replacement.from in sizes[size].types && replacement.to in sizes[size].types) {
			var from = {
				name: replacement.from,
				overhead: sizes[size].types[replacement.from].modeIndicator.length + sizes[size].types[replacement.from].charCountLength,
				bits: typeBits[replacement.from],
				group: typeBits[replacement.from].length - 1
			};
			
			var to = {
				name: replacement.to,
				overhead: sizes[size].types[replacement.to].modeIndicator.length + sizes[size].types[replacement.to].charCountLength,
				bits: typeBits[replacement.to],
				group: typeBits[replacement.to].length - 1
			};
			
			var c = 0, i = 0;
			while (i < modeData[size].length) {
				if (modeData[size][i].type != from.name) {
					i++;
					c = 0;
					continue;
				}
				var length = 0;
				while (inType(modeData[size][i].data.charAt(c + length), to.name)) length++;
				
				var sizeChange = 0;
				if (c == 0) sizeChange -= from.overhead;
				if (c != 0 || i == 0 || modeData[size][i - 1].type != to.name) sizeChange += to.overhead;
				if (c + length != modeData[size][i].data.length) sizeChange += from.overhead;
				else if (i != modeData[size].length - 1 && modeData[size][i + 1].type == to.name) sizeChange -= to.overhead;

				sizeChange -= from.bits[from.group] * Math.floor(length / from.group) + from.bits[length % from.group];
				sizeChange += to.bits[to.group] * Math.floor(length / to.group) + to.bits[length % to.group];
				
				if (sizeChange < 0) {
					var newParts = [ ], newI = i + 1, newC = 0;
					if (c != 0) {
						newI++;
						newParts.push({ type: from.name, data: modeData[size][i].data.substr(0, c) });
					}
					newParts.push({ type: to.name, data: modeData[size][i].data.substr(c, length) });
					if (c + length != modeData[size][i].data.length) newParts.push({ type: from.name, data: modeData[size][i].data.substr(c + length) });
					
					if (newParts.length && i != 0 && newParts[0].type == modeData[size][i - 1].type) {
						newI--;
						modeData[size][i - 1].data = modeData[size][i - 1].data + newParts.shift().data;
					}
					if (newParts.length && i != modeData[size].length - 1 && newParts[newParts.length - 1].type == modeData[size][i + 1].type) {
						if (newI == i + newParts.length) newC = newParts[newParts.length - 1].data.length;
						modeData[size][i + 1].data = newParts.pop().data + modeData[size][i + 1].data;
					}
					
					modeData[size].splice(i, 1, ...newParts);
					i = newI;
					c = newC;
				} else {
					c += length + 1;
					if (c >= modeData[size][i].data.length) {
						i++;
						c = 0;
					}
				}
			}
		}
	
	// Convert to bitStream
	for (var size in modeData) {
		var binString = "";
		for (var part of modeData[size]) {
			binString += sizes[size].types[part.type].modeIndicator;
			binString += part.data.length.toString(2).padStart(sizes[size].types[part.type].charCountLength, "0");
			
			var groupSize = typeBits[part.type].length - 1;
			for (var i = 0; i < part.data.length; i += groupSize) {
				var group = part.data.substr(i, groupSize);
				var groupVal;
				switch (part.type) {
					case 'numeric': groupVal = FromBase(group, "0123456789"); break;
					case 'alphanumeric': groupVal = FromBase(group, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:"); break;
					case 'byte': groupVal = group.charCodeAt(); break;
				}
				binString += groupVal.toString(2).padStart(typeBits[part.type][group.length], "0");
			}
		}
		modeData[size] = binString;
	}
	
	// Select version
	var mode;
	if (!mode && modeData.M1) {
		if (modeData.M1.length <= 8 * (RSBlocks['M1'][0] - RSBlocks['M1'][1][correction][0] * RSBlocks['M1'][1][correction][1]) - (RSBlocks['M1'][2] ? (8 - RSBlocks['M1'][2]) : 0)) {
			mode = 'M1';
			modeData = modeData.M1 + sizes.M1.terminator;
		}
	}
	if (!mode && modeData.M2) {
		if (modeData.M2.length <= 8 * (RSBlocks['M2'][0] - RSBlocks['M2'][1][correction][0] * RSBlocks['M2'][1][correction][1]) - (RSBlocks['M2'][2] ? (8 - RSBlocks['M2'][2]) : 0)) {
			mode = 'M2';
			modeData = modeData.M2 + sizes.M2.terminator;
		}
	}
	if (!mode && modeData.M3) {
		if (modeData.M3.length <= 8 * (RSBlocks['M3'][0] - RSBlocks['M3'][1][correction][0] * RSBlocks['M3'][1][correction][1]) - (RSBlocks['M3'][2] ? (8 - RSBlocks['M3'][2]) : 0)) {
			mode = 'M3';
			modeData = modeData.M3 + sizes.M3.terminator;
		}
	}
	if (!mode && modeData.M4) {
		if (modeData.M4.length <= 8 * (RSBlocks['M4'][0] - RSBlocks['M4'][1][correction][0] * RSBlocks['M4'][1][correction][1]) - (RSBlocks['M4'][2] ? (8 - RSBlocks['M4'][2]) : 0)) {
			mode = 'M4';
			modeData = modeData.M4 + sizes.M4.terminator;
		}
	}
	if (!mode && modeData.V1_9) {
		for (var v = 1; v <= 9; v++)
			if (modeData.V1_9.length <= 8 * (RSBlocks['V'+v][0] - RSBlocks['V'+v][1][correction][0] * RSBlocks['V'+v][1][correction][1]) - (RSBlocks['V'+v][2] ? (8 - RSBlocks['V'+v][2]) : 0)) {
				mode = 'V'+v;
				modeData = modeData.V1_9 + sizes.V1_9.terminator;
				break;
			}
	}
	if (!mode && modeData.V10_26) {
		for (var v = 10; v <= 26; v++)
			if (modeData.V10_26.length <= 8 * (RSBlocks['V'+v][0] - RSBlocks['V'+v][1][correction][0] * RSBlocks['V'+v][1][correction][1]) - (RSBlocks['V'+v][2] ? (8 - RSBlocks['V'+v][2]) : 0)) {
				mode = 'V'+v;
				modeData = modeData.V10_26 + sizes.V10_26.terminator;
				break;
			}
	}
	if (!mode && modeData.V27_40) {
		for (var v = 27; v <= 40; v++)
			if (modeData.V27_40.length <= 8 * (RSBlocks['V'+v][0] - RSBlocks['V'+v][1][correction][0] * RSBlocks['V'+v][1][correction][1]) - (RSBlocks['V'+v][2] ? (8 - RSBlocks['V'+v][2]) : 0)) {
				mode = 'V'+v;
				modeData = modeData.V27_40 + sizes.V27_40.terminator;
				break;
			}
	}
	if (!mode) return { error: "No valid version. (Try using a different correction level or removing some data)" };
	
	// Split into blocks & codewords
	modeData += '0'.repeat(modeData.length % 8);
	for (var padNo = 0, padCount = RSBlocks[mode][0] - RSBlocks[mode][1][correction][0] * RSBlocks[mode][1][correction][1] - modeData.length / 8; padNo < padCount; padNo++)
		modeData += padNo == padCount - 1 && RSBlocks[mode][2] ? '0'.repeat(RSBlocks[mode][2]) : [ '11101100', '00010001' ][padNo % 2];
	modeData = modeData.substring(0, 8 * (RSBlocks[mode][0] - RSBlocks[mode][1][correction][0] * RSBlocks[mode][1][correction][1]) - (RSBlocks[mode][2] ? (8 - RSBlocks[mode][2]) : 0));
	var dataBlocks = [ ];
	for (var words = Math.ceil(modeData.length / 8), blocks = RSBlocks[mode][1][correction][0], w = 0, b = 0; b < blocks; b++) {
		var l = Math.floor((words - w) / (blocks - b));
		var codewords = [ ];
		for (var nextW = w + l; w < nextW; w++)
			codewords.push(modeData.substr(w * 8, 8));
		dataBlocks.push(codewords);
	}
	
	// Generate error correction codewords https://en.wikiversity.org/wiki/Reed%E2%80%93Solomon_codes_for_coders
	var correctionBlocks = [ ];
	var gen = Array(RSBlocks[mode][1][correction][1]).fill(0).reduce((p, _, i) => gf.poly_mult(p, [1, gf.exp[i]]), [ 1 ]);
	for (var b = 0; b < dataBlocks.length; b++)
		correctionBlocks[b] = gf.poly_rem(dataBlocks[b].map(b => parseInt(b, 2)).concat(Array(RSBlocks[mode][1][correction][1]).fill(0)), gen)
			.map(b => b.toString(2).padStart(8, '0'));
	
	// Combine data and error correction blocks into one stream
	var stream = "";
	for (var w = 0, words = dataBlocks[dataBlocks.length - 1].length; w < words; w++)
		for (var b = 0, blocks = dataBlocks.length; b < blocks; b++)
			if (dataBlocks[b][w]) stream += dataBlocks[b][w];
	for (var w = 0, words = RSBlocks[mode][1][correction][1]; w < words; w++)
		for (var b = 0, blocks = correctionBlocks.length; b < blocks; b++)
			stream += correctionBlocks[b][w];
	
	// Create arrays
	var QR = [ ];
	var Data = [ ];
	var version = Number(mode.substring(1));
	var size = mode.charAt(0) == 'V' ? 17 + version * 4 : 9 + version * 2;
	for (var x = 0; x < size; x++) {
		QR[x] = Array(size);
		Data[x] = Array(size);
	}
	
	// Place function things
	function placeFinder(cx, cy) {
		for (var r = 0; r < 5; r++)
			for (var dx = -r; dx <= r; dx++) if (cx + dx >= 0 && cx + dx < size)
				for (var dy = -r; dy <= r; dy++) if (cy + dy >= 0 && cy + dy < size)
					if (QR[cx + dx][cy + dy] == undefined)
						QR[cx + dx][cy + dy] = !r || r % 2 ? 1 : 0;
	}
	function placeAlign(cx, cy) {
		for (var r = 0; r < 3; r++)
			for (var dx = -r; dx <= r; dx++) if (cx + dx >= 0 && cx + dx < size)
				for (var dy = -r; dy <= r; dy++) if (cy + dy >= 0 && cy + dy < size)
					if (QR[cx + dx][cy + dy] == undefined)
						QR[cx + dx][cy + dy] = r % 2 ? 0 : 1;
	}
	function placeTiming(d) {
		for (var l = 0; l < size; l++) {
			if (QR[l][d] == undefined) QR[l][d] = l % 2 ? 0 : 1;
			if (QR[d][l] == undefined) QR[d][l] = l % 2 ? 0 : 1;
		}
	}
	function placeInfo(x, y, w, h) {
		for (var dx = 0; dx < w; dx++)
			for (var dy = 0; dy < h; dy++)
				if (QR[x + dx][y + dy] == undefined) QR[x + dx][y + dy] = 0;
	}
	switch (mode.charAt(0)) {
		case 'M':
			placeFinder(3, 3);
			placeTiming(0);
			placeInfo(0, 0, 9, 9);
			break;
		case 'V':
			placeFinder(3, 3);
			placeFinder(3, size - 4);
			placeFinder(size - 4, 3);
			placeTiming(6);
			placeInfo(0, 0, 9, 9);
			placeInfo(size - 8, 8, 8, 1);
			placeInfo(8, size - 8, 1, 8);
			if (version >= 7) {
				placeInfo(size - 11, 0, 3, 6);
				placeInfo(0, size - 11, 6, 3);
			}
			var count = version == 1 ? 0 : Math.floor(version / 7) + 1;
			var dist = [
				 0,  0,  0,  0,  0,  0,  0,
				16, 18, 20, 22, 24, 26, 28,
				20, 22, 24, 24, 26, 28, 28,
				22, 24, 24, 26, 26, 28, 28,
				24, 24, 26, 26, 26, 28, 28,
				24, 26, 26, 26, 28, 28
			][version];
			for (var x = 0; x <= count; x++)
				for (var y = 0; y <= count; y++)
					if (!(x == 0 && y == 0 || x == count && y == 0 || x == 0 && y == count))
						placeAlign(x == 0 ? 6 : size - 7 - (count - x) * dist, y == 0 ? 6 : size - 7 - (count - y) * dist);
			break;
	}
	
	// Place data in data array
	for (var b = 0, c = size - 1, y = size - 1, dir = -1, x = 0; c > 0; b++) {
		Data[c - x][y] = Number(stream.charAt(b));
		do {
			if (++x > 1) {
				x = 0;
				y += dir;
				if (y < 0 || y >= size) {
					y += (dir = -dir);
					c -= 2;
					if (c > x) {
						var full = true;
						for (var i = 0; i < size; i++) {
							if (QR[c - x][i] == undefined) {
								full = false;
								break;
							}
						}
						if (full) c -= 1;
					}
				}
			}
		} while (c > 0 && QR[c - x][y] != undefined);
	}
	
	// Calculate score for each mask
	var bestMask, bestScore;
	var getCell = (x, y, m) => QR[x][y] != undefined ? QR[x][y] : Data[x][y] ^ masks[mode.charAt(0)][m](x, y); 
	for (var m = 0; m < masks[mode.charAt(0)].length; m++) {
		var score = 0;
		if (mode.charAt(0) == 'V') {
			var onCount = 0;
			for (var x = 0; x < size; x++)
				for (var y = 0; y < size; y++) {
					var cell = getCell(x, y, m);
					if (x == 0 || cell != getCell(x - 1, y, m)) {
						var s;
						for (s = 1; x + s < size && getCell(x + s, y, m) == cell; s++);
						if (s >= 5) score -= 3 + (s - 5);
					}
					if (y == 0 || cell != getCell(x, y - 1, m)) {
						var s;
						for (s = 1; y + s < size && getCell(x, y + s, m) == cell; s++);
						if (s >= 5) score -= 3 + (s - 5);
					}
					if (x + 1 < size && y + 1 < size &&
						[[0,1],[1,0],[1,1]].every(p => getCell(x + p[0], y + p[1], m) == cell))
						score -= 3;
					if (size - x >= 11 &&
						([0,0,0,0,1,0,1,1,1,0,1].every((v, i) => getCell(x + i, y, m) == v) ||
						[1,0,1,1,1,0,1,0,0,0,0].every((v, i) => getCell(x + i, y, m) == v)))
							score -= 40;
					if (size - y >= 11 &&
						([0,0,0,0,1,0,1,1,1,0,1].every((v, i) => getCell(x, y + i, m) == v) ||
						[1,0,1,1,1,0,1,0,0,0,0].every((v, i) => getCell(x, y + i, m) == v)))
							score -= 40;
					onCount += cell;
				}
			score -= Math.floor(Math.abs(onCount / (size * size) - 0.5) * 20) * 10;
		} else {
			var sum1 = 0, sum2 = 0;
			for (var i = 1; i < size; i++) {
				sum1 += getCell(size - 1, i, m);
				sum2 += getCell(size - 1, i, m);
			}
			if (sum1 > sum2) score += sum1 + sum2 * 16;
			else score += sum2 + sum1 * 16;
		}
		
		if (!(bestScore >= score)) {
			bestMask = m;
			bestScore = score;
		}
	}
	
	// Merge format, data and mask
	for (var x = 0; x < size; x++)
		for (var y = 0; y < size; y++)
			if (QR[x][y] == undefined) QR[x][y] = getCell(x, y, bestMask);
	
	// Get format info
	var fInfo;
	if (mode.charAt(0) == 'V') fInfo = ({
		l: 0b01,
		m: 0b00,
		q: 0b11,
		h: 0b10
	}[correction] << 3) + bestMask;
	else fInfo = ({
		M1n: 0b000,
		M2l: 0b001,
		M2m: 0b010,
		M3l: 0b011,
		M3m: 0b100,
		M4l: 0b101,
		M4m: 0b110,
		M4q: 0b111
	}[mode + correction] << 2) + bestMask;
	
	// Calculate error correction for format info
	fInfo <<= 10;
	fInfo += gf.bin_poly_rem(fInfo, 0b10100110111);
	fInfo ^= mode.charAt(0) == 'V' ? 0b101010000010010 : 0b100010001000101;
	fInfo = fInfo.toString(2).padStart(15, '0');
	
	// Place format info
	for (var x = 8, y = -1, b = 0; b < 15; b++) {
		do {
			if (y < 8) y++;
			else x--;
		} while (QR[x][y] != 0);
		QR[x][y] = Number(fInfo.charAt(14 - b));
	}
	if (mode.charAt(0) == 'V') {
		QR[8][size - 8] = 1;
		for (var b = 0; b < 15; b++) {
			if (b < 8) QR[size - 1 - b][8] = Number(fInfo.charAt(14 - b));
			else QR[8][size - (15 - b)] = Number(fInfo.charAt(14 - b));
		}
	}
	
	if (mode.charAt(0) == 'V' && version >= 7) {
		// Calculate error correction for version info
		var vInfo = version << 12;
		vInfo += gf.bin_poly_rem(vInfo, 0b1111100100101);
		vInfo = vInfo.toString(2).padStart(18, '0');
		
		// Place version info
		for (var b = 0; b < 18; b++) {
			QR[(size - 11) + (b % 3)][Math.floor(b / 3)] = Number(vInfo.charAt(17 - b));
			QR[Math.floor(b / 3)][(size - 11) + (b % 3)] = Number(vInfo.charAt(17 - b));
		}
	}
	
	return { code: QR, version: mode, size: size, padding: mode.charAt(0) == 'V' ? 4 : 2 };
}

function FromBase(number, base) {
	var result = 0;
	for (var i = 0; i < number.length; i++) {
		var value = base.indexOf(number.charAt(i));
		if (value < 0) return -1;
		result = result * base.length + value;
	}
	return result;
};
